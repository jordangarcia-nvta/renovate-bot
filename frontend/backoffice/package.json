{
  "name": "frontend",
  "version": "0.0.0",
  "description": "",
  "private": true,
  "scripts": {
    "precommit": "lint-staged",
    "postinstall": "bash ../lib/consumer-postinstall.sh backoffice",
    "test": "jest",
    "test:debug": "node --inspect-brk ./node_modules/.bin/jest --runInBand",
    "test:coverage": "jest --coverage",
    "test:watch": "jest --watch",
    "pact:test": "jest --config ./test/pact/jestconfig.json",
    "pact:publish": "node ./test/pact/publish.js",
    "pact:verify": "jest --config ./test/pact/jestVerifyConfig.json",
    "start": "webpack-dev-server --config webpack.config.dev.js",
    "cypress": "cypress run",
    "cypress:open": "cypress open",
    "cypress:serve": "NODE_ENV=test webpack-dev-server --config webpack.config.dev.js",
    "build": "NODE_ENV=production webpack --config webpack.config.prod.js --progress",
    "lint": "eslint --ext .js,.jsx src cypress test",
    "lint:fix": "eslint --fix --ext .js,.jsx src cypress test",
    "lint:prettier-fix": "./node_modules/.bin/prettier prettier --config ./package.json --write '{src,cypress,test}/**/*.js'"
  },
  "lint-staged": {
    "*.js": [
      "eslint"
    ]
  },
  "dependencies": {
    "@invitae/pdfjs": "^2.0.943",
    "baobab": "2.4.3",
    "baobab-react": "2.1.2",
    "classnames": "1.2.2",
    "fixed-data-table": "0.6.5",
    "jquery": "3.3.1",
    "lodash": "4.17.11",
    "material-ui": "0.19.4",
    "moment": "2.22.2",
    "moment-timezone": "^0.5.14",
    "normalize.css": "3.0.2",
    "numeral": "^2.0.6",
    "prop-types": "^15.6.0",
    "qs": "6.5.0",
    "react": "15.6.1",
    "react-addons-linked-state-mixin": "15.6.0",
    "react-addons-pure-render-mixin": "15.6.0",
    "react-addons-test-utils": "15.6.0",
    "react-datepicker": "^0.60.2",
    "react-dom": "15.6.1",
    "react-dropzone": "^4.3.0",
    "react-fa": "4.2.0",
    "react-maskedinput": "4.0.0",
    "react-notification-system": "0.2.15",
    "react-redux": "5.0.6",
    "react-router": "4.2.0",
    "react-router-dom": "4.2.2",
    "react-tap-event-plugin": "2.0.1",
    "react-tooltip": "3.9.2",
    "redux": "3.7.2",
    "redux-form": "7.1.1",
    "redux-logger": "3.0.6",
    "whatwg-fetch": "3.0.0"
  },
  "devDependencies": {
    "@pact-foundation/pact": "8.1.0",
    "babel": "^6.23.0",
    "babel-eslint": "^9.0.0",
    "babel-jest": "20.0.3",
    "babel-loader": "7.1.5",
    "babel-plugin-transform-class-properties": "^6.24.1",
    "babel-plugin-transform-object-rest-spread": "6.26.0",
    "babel-polyfill": "6.26.0",
    "babel-preset-env": "^1.7.0",
    "babel-preset-react": "6.24.1",
    "babel-preset-stage-2": "^6.24.1",
    "css-loader": "0.28.7",
    "cypress": "2.1.0",
    "dotenv": "4.0.0",
    "enzyme": "^3.6.0",
    "enzyme-adapter-react-15": "^1.1.0",
    "enzyme-to-json": "^3.3.4",
    "eslint": "^5.5.0",
    "eslint-config-prettier": "^3.0.1",
    "eslint-config-standard": "^12.0.0",
    "eslint-loader": "^2.1.0",
    "eslint-plugin-cypress": "^2.0.1",
    "eslint-plugin-import": "^2.14.0",
    "eslint-plugin-jest": "^21.22.0",
    "eslint-plugin-node": "^7.0.1",
    "eslint-plugin-prettier": "^2.6.2",
    "eslint-plugin-promise": "^4.0.1",
    "eslint-plugin-react": "^7.11.1",
    "eslint-plugin-standard": "^4.0.0",
    "fetch-mock": "7.2.1",
    "file-loader": "2.0.0",
    "html-webpack-plugin": "3.2.0",
    "identity-obj-proxy": "3.0.0",
    "jeet": "6.1.2",
    "jest": "23.1.0",
    "jest-junit": "5.1.0",
    "jsdom": "13.0.0",
    "less": "3.8.1",
    "less-loader": "4.1.0",
    "lint-staged": "7.2.2",
    "nib": "1.1.2",
    "prettier": "^1.14.2",
    "react-hot-loader": "3.1.1",
    "react-test-renderer": "^15.6.1",
    "style-loader": "0.23.1",
    "stylus": "0.54.5",
    "stylus-loader": "3.0.2",
    "uglifyjs-webpack-plugin": "2.0.1",
    "url-loader": "1.1.2",
    "webpack": "4.25.1",
    "webpack-bundle-clean": "0.0.7",
    "webpack-bundle-tracker": "0.3.0",
    "webpack-cli": "3.1.2",
    "webpack-dev-server": "3.1.10",
    "webpack-merge": "4.1.4"
  },
  "jest": {
    "testMatch": [
      "**/__test__/**/*_tests.js"
    ],
    "transform": {
      "^.+\\.js$": "babel-jest"
    },
    "modulePaths": [
      "node_modules",
      "<rootDir>/src",
      "<rootDir>/test"
    ],
    "moduleFileExtensions": [
      "js"
    ],
    "moduleNameMapper": {
      "\\.(css|styl|less)$": "identity-obj-proxy",
      "^.+pdf\\.worker\\.min$": "<rootDir>/test/helpers/mocks/pdf.worker.mock.js"
    },
    "setupFiles": [
      "<rootDir>/test/helpers/configureEnzyme.js",
      "<rootDir>/test/helpers/environment.js"
    ],
    "setupTestFrameworkScriptFile": "<rootDir>/test/helpers/consoleOverride.js",
    "collectCoverageFrom": [
      "src/**/*.{js,jsx}",
      "!**/node_modules/**",
      "!**/vendor/**"
    ],
    "coverageDirectory": "<rootDir>/test/coverage",
    "coverageReporters": [
      "html",
      "text"
    ],
    "reporters": [
      "default",
      [
        "jest-junit",
        {
          "output": "junit/TESTS.xml"
        }
      ]
    ]
  },
  "eslintIgnore": [
    "../lib",
    "dist",
    "test/coverage"
  ],
  "prettier": {
    "printWidth": 120,
    "tabWidth": 2,
    "useTabs": false,
    "semi": false,
    "singleQuote": true,
    "trailingComma": "none",
    "bracketSpacing": false
  },
  "config": {
    "pact_do_not_track": true
  }
}
