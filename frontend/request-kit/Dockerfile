ARG REGISTRY=docker-repo.vpc.locusdev.net
ARG TAG=latest
FROM ${REGISTRY}/web/web_commons:${TAG} as builder

ENV WEB_COMMONS_DIR $APP_DIR
ENV APP_DIR /locus-web/apps/portal-rkt

WORKDIR $APP_DIR

COPY package.json $APP_DIR/package.json
COPY package-lock.json $APP_DIR/package-lock.json

RUN DISABLE_INVITAE_WEB_COMMONS_POSTINSTALL=1 npm --no-color ci
RUN mv $WEB_COMMONS_DIR node_modules/

COPY . $APP_DIR
COPY example.env .env

RUN CI=true npm --no-color test
RUN CI=true npm --no-color run test:pact

## Build app
RUN CI=true npm --no-color run build

FROM ${REGISTRY}/web/busybox:latest

ENV APP_DIR /locus-web/apps/portal-rkt
ENV RKT_STATIC_DIR /locus-web/apps/invitae-web-portal/portal/static/rk_compiled
ENV RKT_STATIC_ALL_DIR /locus-web/apps/invitae-web-portal/portal/static_all/rk_compiled

COPY --from=builder $APP_DIR/build $RKT_STATIC_DIR
COPY --from=builder $APP_DIR/build $RKT_STATIC_ALL_DIR
COPY --from=builder $APP_DIR/junit $APP_DIR/junit
COPY --from=builder $APP_DIR/pacts /pacts

VOLUME $RKT_STATIC_DIR
VOLUME $RKT_STATIC_ALL_DIR

WORKDIR /

ARG GIT_SHA=NOT_SET
ARG BUILD_DATE=NOT_SET
ARG TAG=latest
RUN echo "SHA=${GIT_SHA}" > build.info
RUN echo "DATE=${BUILD_DATE}" >> build.info
RUN echo "PKG_VERSION=${TAG}" >> build.info
